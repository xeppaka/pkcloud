# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.kernelPackages = pkgs.linuxPackages_latest;
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.kernel.sysctl."net.ipv4.ip_forward" = 1;
  boot.supportedFilesystems = [ "ntfs" ];

  networking.hostName = "pkcloud"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  # Set your time zone.
  time.timeZone = "Europe/Prague";

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    wget
    vim
    btrfs-progs
    gptfdisk
    docker
    docker_compose
    git
    letsencrypt
    vlc
    usbutils
    lm_sensors
    openjdk8
    smartmontools
    mdadm
    openssl
    cfssl
    kubectl
    kompose
    bind
    easyrsa
    openvpn
    kubernetes
    helm
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  programs.bash.enableCompletion = true;
  # programs.mtr.enable = true;
  # programs.gnupg.agent = { enable = true; enableSSHSupport = true; };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;
  services.openssh.permitRootLogin = "prohibit-password";

  services.openvpn.servers = {
    server = {
      config = ''
        dev        tun
        port       1194
        ca         /usr/apps/openvpn/server/ca.crt
        cert       /usr/apps/openvpn/server/xeppaka.eu.crt
        key        /usr/apps/openvpn/server/xeppaka.eu.key
        dh         /usr/apps/openvpn/server/dh.pem
        tls-auth   /usr/apps/openvpn/server/ta.key 0
        crl-verify /usr/apps/openvpn/server/crl.pem
        topology   subnet
        local      192.168.1.5
        server     10.8.0.0 255.255.255.0
        proto      udp
        client-to-client
        log        /usr/apps/openvpn/log/openvpn.log
        verb       3
        status     /usr/apps/openvpn/log/openvpn-status.log
      '';
    };
  };

  services.smartd = {
    enable = true;
    extraOptions = [ "-A /var/log/smartd/" "-i 600"];
  };

  virtualisation.docker = {
    enable = true;
    enableOnBoot = true;
  };

  services.kubernetes = {
    roles = [ "master" "node" ];
    addons.dashboard.enable = true;
    apiserver = {
      enable = true;
      insecurePort = 8080;
      securePort = 6443;
    };
    masterAddress = "pkcloud";
    easyCerts = true;
  };

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [ 80 88 443 8778 8443 222 111 2049 9090 6443 51413 ];
  networking.firewall.allowedUDPPorts = [ 111 2049 1194 51413 ];
  networking.firewall.extraCommands = ''
    iptables -t nat -A POSTROUTING -s 10.8.0.0/24 -o enp3s0 -j MASQUERADE
  '';
  # Or disable the firewall altogether.
  #networking.firewall.enable = false;

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  # sound.enable = true;
  # hardware.pulseaudio.enable = true;

  # Enable the X11 windowing system.
  # services.xserver.enable = true;
  # services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e";

  # Enable touchpad support.
  # services.xserver.libinput.enable = true;

  # Enable the KDE Desktop Environment.
  # services.xserver.displayManager.sddm.enable = true;
  # services.xserver.desktopManager.plasma5.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.extraUsers.nnm = {
    isNormalUser = true;
    uid = 1000;
    shell = pkgs.fish;
    extraGroups = [ "video" ];
  };

  nixpkgs.config = {
    allowUnfree = true;
  };

  services.nfs.server.enable = true;
  services.nfs.server.exports = ''
    /usr/storage0001/export                   192.168.1.0/24(insecure,rw,fsid=0,no_subtree_check) 10.8.0.0/24(insecure,rw,fsid=0,no_subtree_check)
    /usr/storage0001/export/downloads         192.168.1.0/24(insecure,rw,nohide,no_subtree_check) 10.8.0.0/24(insecure,rw,nohide,no_subtree_check)
    /usr/storage0001/export/torrent-watch     192.168.1.0/24(insecure,rw,nohide,no_subtree_check) 10.8.0.0/24(insecure,rw,nohide,no_subtree_check)
    /usr/storage0001/export/mp-1              192.168.1.0/24(insecure,rw,nohide,no_subtree_check) 10.8.0.0/24(insecure,rw,nohide,no_subtree_check)
  '';

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "20.03"; # Did you read the comment?
}

